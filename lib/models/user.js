var debug   = require("debug")("relais-3:user")
  , redis   = require("redis")
  , async   = require("async")
  , uuid    = require("node-uuid")
  , bcrypt  = require("bcrypt")
  , __      = require("underscore");

var db = null;

/**
 * Initialize the model.
 */
module.exports.init = function () {
  var args = [].slice.call(arguments);

  db = args.shift();

  debug("model initialized");
};

/**
 * User model.
 */
var User = module.exports.model = function () {
  if (!arguments) throw new Error("invalid arguments");

  var args = [].slice.call(arguments);
  __.extend(this, args.shift());
};

/**
 * Create separate datastore connection for pub/sub.
 *
 * Returns true on success, otherwise error.
 */
User.prototype.init = function (callback) {
  this.sub = redis.createClient(
    db.port,
    db.host,
    {
      auth_pass: db.options.auth_pass
    }
  );

  this.sub.on("error", function (err) {
    debug("error", err.message);
    return callback(err);
  });

  this.sub.on("end", function () {
    debug("disconnected from redis datastore");
    return callback(null, false);
  });

  this.sub.on("connect", function () {
    debug("connected to redis datastore");
    return callback(null, true);
  });
};

/**
 * Create new user.
 *
 * Returns created user or error.
 */
User.prototype.create = function (callback) {
  var self = this;

  async.waterfall([
    function (cb) {
      findByUsername(self.username, function (err, user) {
        if (err) cb(err);
        cb(null, user);
      });
    },
    function (user, cb) {
      if (user) {
        cb(new Error("Benutzer existiert bereits!"));
      }

      db.INCR("USERID", function (err, id) {
        if (err) cb(err);
        cb(null, id);
      });
    },
    function (id, cb) {
      bcrypt.hash(self.password, 10, function (err, hash) {
        if (err) cb(err);
        cb(null, id, hash);
      });
    },
    function (id, hash, cb) {
      db.MULTI()
        .SET("id:" + id + ":username",  self.username)
        .SET("id:" + id + ":hash",      hash)
        .SET("id:" + id + ":when",      new Date().getTime())
        .SADD("users", id)
      .EXEC(function (err, reply) {
        if (err) cb(err);
        cb(null, id, hash);
      });
    }
  ], function (err, id, hash) {
    if (err) return callback(err);

    self.id = id;
    self.hash = hash;
    debug("created", self.id, self.username);
    return callback(null, self);
  });
};

/**
 * Updates user.
 *
 * Returns updated user or error.
 */
User.prototype.update = function (updates, callback) {
  var self = this;

  async.waterfall([
    function (cb) {
      findByUsername(self.username, function (err, user) {
        if (err) cb(err);
        __.extend(self, updates);
        cb(null, user);
      });
    },
    function (user, cb) {
      if (!user) {
        cb(new Error("Benutzer existiert nicht!"));
      }
      cb(null, user.id);
    },
    function (id, cb) {
      bcrypt.hash(self.password, 10, function (err, hash) {
        if (err) cb(err);
        cb(null, id, hash);
      });
    },
    function (id, hash, cb) {
      db.MULTI()
        .SET("id:" + id + ":username",  self.username)
        .SET("id:" + id + ":hash",      hash)
        .SET("id:" + id + ":when",      new Date().getTime())
      .EXEC(function (err, reply) {
        if (err) cb(err);
        cb(null, id, hash);
      });
    }
  ], function (err, id, hash) {
    if (err) return callback(err);

    self.id = id;
    self.hash = hash;
    debug("updated", self.id, self.username);
    return callback(null, self);
  });
};

/**
 * Delete user.
 *
 * Returns redis reply or error.
 */
User.prototype.delete = function (callback) {
  var self = this;

  db.MULTI()
    .DEL([
      "id:" + self.id + ":username",
      "id:" + self.id + ":hash",
      "id:" + self.id + ":when"
    ])
    .SREM("users", self.id)
  .EXEC(function (err, reply) {
    if (err) return callback(err);

    return callback(null, reply);
  });
};

/**
 * Authenticate user.
 *
 * Returns authenticated user or error.
 */
User.prototype.authenticate = function (password, callback) {
  var self = this;
  self.check(password, function (err, valid) {
    if (err) return callback(err);

    if (valid) {
      self.sessionId = valid;
      return callback(null, self);
    }

    self.password = password;
    bcrypt.compare(self.password, self.hash, function (err, matches) {
      if (err) return callback(err);

      if (!matches) return callback(new Error("Authentifizierung fehlgeschlagen!"));

      self.sessionId = uuid.v4();
      db.MULTI()
        .SET("id:" + self.id + ":sessionId", self.sessionId)
        .SET("id:" + self.id + ":expires",   new Date().getTime() + new Date(15*60*1000).getTime())
      .EXEC(function (err, reply) {
        if (err) return callback(err);
        return callback(null, self);
      });
    });
  });
};

/**
 * Check for valid session.
 *
 * Returns sessionId or error.
 */
User.prototype.check = function (sessionId, callback) {
  var self = this;

  var validate = function (uuid) {
    var regex = new RegExp("^[a-f0-9]{8}-?[a-f0-9]{4}-?4[a-f0-9]{3}-?[ab89][a-f0-9]{3}-?[a-f0-9]{12}$");
    return uuid.match(regex);
  };

  async.parallel([
    function (cb) {
      db.GET("id:" + self.id + ":sessionId", function (err, reply) {
        if (err) cb(err);
        cb(null, reply);
      });
    },
    function (cb) {
      db.GET("id:" + self.id + ":expires", function (err, reply) {
        if (err) cb(err);
        cb(null, reply);
      });
    }
  ], function (err, results) {
    if (err) return callback(err);
    var expired = results[1] - new Date().getTime() <= 0;
    if (expired || !validate(sessionId)) {
      return callback(null, null);
    }
    db.SET("id:" + self.id + ":expires", new Date().getTime() + new Date(15*60*1000).getTime(), function (err, reply) {
      if (err) return callback(err);
      return callback(null, results[0]);
    });
  });
};

/**
 * Retrieve user from datastore.
 */
var find = module.exports.find = function (id, callback) {
  var user = new User({ id: id });

  async.parallel([
    function (cb) {
      db.GET("id:" + user.id + ":username", function (err, reply) {
        if (err) cb(err);
        user.username = reply;
        cb();
      });
    },
    function (cb) {
      db.GET("id:" + user.id + ":hash", function (err, reply) {
        if (err) cb(err);
        user.hash = reply;
        cb();
      });
    },
    function (cb) {
      db.GET("id:" + user.id + ":when", function (err, reply) {
        if (err) cb(err);
        user.when = reply;
        cb();
      });
    }
  ], function (err) {
    if (err) return callback(err);

    return callback(null, user);
  });
};

/**
 * Retrieve user by name.
 */
var findByUsername = module.exports.findByUsername = function (username, callback) {
  db.SMEMBERS("users", function (err, list) {
    if (err) return callback(err);

    var foundUser = null;
    async.detect(list, function (id, cb) {
      find(parseInt(id, 10), function (err, user) {
        if (err) return callback(err);

        var found = user.username === username;
        if (found) {
          foundUser = user;
        }
        cb(found);
      });
    }, function (id) {
      return callback(null, foundUser);
    });
  });
};
