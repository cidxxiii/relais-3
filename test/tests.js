var should    = require("should")
  , debug     = require("debug")("relais-3:models")
  , redis     = require("redis")
  , WebSocket = require("ws")
  , argv      = require("optimist").demand("config").argv
  , config    = require(argv.config)
  , User      = require("../lib/models").User
  , __        = require("underscore");

var db   = null
  , ws   = null
  , user = null;

describe("User", function () {
  before(function (done) {
    db = redis.createClient(
      config.redis.port,
      config.redis.host,
      {
        auth_pass: config.redis.auth
      }
    );

    db.on("connect", function () {
      User.init(db);
      db.FLUSHDB();
      db.SET("USERID", 1000, done);
    });
  });

  describe("#User()", function () {
    it("should pass without errors", function (done) {
      db.connected.should.be.ok;

      user = new User.model({
        username: "karma",
        password: "s3cr3t"
      });

      user.should.be.ok;
      user.username.should.equal("karma");
      user.password.should.equal("s3cr3t");
      done();
    });
  });

  describe("#init()", function () {
    it("should pass without errors", function (done) {
      db.connected.should.be.ok;

      user.init(function (err, reply) {
        if (err) throw err;
        user.sub.should.be.ok;
        user.sub.connected.should.be.ok;
        done();
      });
    });
  });

  describe("#save()", function () {
    it("should pass without errors", function (done) {
      db.connected.should.be.ok;

      user.create(function (err, reply) {
        if (err) throw err;
        user = reply;
        user.should.be.ok;
        user.id.should.equal(1001);
        user.hash.should.be.ok;
        done();
      });
    });
  });

  describe("#update()", function () {
    it("should pass without errors", function (done) {
      db.connected.should.be.ok;

      user.update({
        username: "beta",
        password: "tester"
      }, function (err, reply) {
        if (err) throw err;
        user = reply;
        user.should.be.ok;
        user.id.should.equal(1001);
        user.username.should.equal("beta");
        user.password.should.equal("tester");
        user.hash.should.be.ok;
        done();
      });
    });
  });

  describe("find()", function () {
    it("should return test user", function (done) {
      db.connected.should.be.ok;

      User.find(1001, function (err, reply) {
        if (err) throw err;

        user = reply;
        user.username.should.equal("beta");
        user.hash.should.be.ok;
        done();
      });
    });
  });

  describe("findByUsername()", function () {
    it("should return test user", function (done) {
      db.connected.should.be.ok;

      User.findByUsername("beta", function (err, reply) {
        if (err) throw err;

        user = reply;
        user.should.be.ok;
        user.username.should.equal("beta");
        user.hash.should.be.ok;
        done();
      });
    });
  });

  describe("#authenticate()", function () {
    it("should return authenticated user", function (done) {
      db.connected.should.be.ok;

      user.authenticate("tester", function (err, auth) {
        if (err) throw err;
        auth.should.be.ok;
        auth.id.should.equal(1001);
        auth.sessionId.should.be.ok;
        user = auth;
        done();
      });
    });
  });

  describe("#delete()", function () {
    it("should pass without errors", function (done) {
      db.connected.should.be.ok;

      user.delete(function (err, reply) {
        if (err) throw err;
        reply.should.be.ok;
        done();
      });
    });
  });
});

describe("Server", function () {
  var sessionId = null
    , count = 0;

  before(function (done) {
    WebSocket.prototype.request = function () {
      var args = [].slice.call(arguments);

      if (this.readyState === 1) {
        this.send(JSON.stringify(args));
      }
    };

    ws = new WebSocket("wss://" + config.host + ":" + config.port);

    ws.on("open", function () {
      db.FLUSHDB();
      db.SET("USERID", 1000, function (err, reply) {
        done();
      });
    });
  });

  describe("register user", function () {
    it("should response with sessionId", function (done) {
      ws.readyState.should.equal(1);

      var callback = function (data) {
        var message = JSON.parse(data);

        message.shift().should.equal(12);
        message.shift().should.be.ok;
        ws.removeListener("message", callback);
        done();
      };
      ws.on("message", callback);

      ws.request(10, "karma", "s3cr3t");
    });
  });

  describe("register user twice", function () {
    it("should response with error", function (done) {
      ws.readyState.should.equal(1);

      var callback = function (data) {
        var message = JSON.parse(data);

        message.shift().should.equal(0);
        message.shift().should.be.ok;
        ws.removeListener("message", callback);
        done();
      };
      ws.on("message", callback);

      ws.request(10, "karma", "s3cr3t");
    });
  });

  describe("authenticate user", function () {
    it("should response with sessionId", function (done) {
      ws.readyState.should.equal(1);

      var callback = function (data) {
        var message = JSON.parse(data);

        message.shift().should.equal(12);
        sessionId = message.shift();
        sessionId.should.be.ok;
        ws.removeListener("message", callback);
        done();
      };
      ws.on("message", callback);

      ws.request(11, "karma", "s3cr3t");
    });

    it("should response with an error message if user doesn't exists", function (done) {
      ws.readyState.should.equal(1);

      var callback = function (data) {
        var message = JSON.parse(data);

        message.shift().should.equal(0);
        message.shift().should.be.ok;
        ws.removeListener("message", callback);
        done();
      };
      ws.on("message", callback);

      ws.request(11, "beta", "tester");
    });
  });

  describe("receive channel list", function () {
    it("should response with channel list", function (done) {
      ws.readyState.should.equal(1);

      var callback = function (data) {
        var message = JSON.parse(data);

        message.shift().should.equal(22);
        JSON.parse(message.shift()).length.should.equal(1);
        ws.removeListener("message", callback);
        done();
      };
      ws.on("message", callback);

      ws.request(21, sessionId);
    });
  });

  describe("subscribe channel", function () {
    it("should response with channel", function (done) {
      ws.readyState.should.equal(1);

      var callback = function (data) {
        var message = JSON.parse(data);

        message.shift().should.equal(15);
        message.shift().should.equal("karma");
        message.shift().should.equal("universe");
        message.shift().should.equal(1);
        ws.removeListener("message", callback);
        done();
      };
      ws.on("message", callback);

      sessionId.should.be.ok;
      ws.request(14, sessionId, "universe");
    });
  });

  describe("publish to channel", function () {
    it("should response with published message", function (done) {
      ws.readyState.should.equal(1);

      var callback = function (data) {
        var message = JSON.parse(data);

        switch (message.shift()) {
          case 17:
            message.shift().should.equal("universe");
            message.shift().should.be.ok;
            count++;
            break;
          case 18:
            message.shift().should.equal("karma");
            message.shift().should.equal("universe");
            message.shift().should.be.ok;
            message.shift().should.equal("published message");
            count++;
            break;
          default:
            throw new Error("invalid response type");
        }

        if (count === 2) {
          count = 0;
          ws.removeListener("message", callback);
          done();
        }
      };
      ws.on("message", callback);

      sessionId.should.be.ok;
      ws.request(16, sessionId, "universe", "published message");
    });
  });

  describe("unsubscribe channel", function () {
    it("should response success", function (done) {
      ws.readyState.should.equal(1);

      var callback = function (data) {
        var message = JSON.parse(data);

        message.shift().should.equal(1);
        message.shift().should.be.ok;
        ws.removeListener("message", callback);
        done();
      };
      ws.on("message", callback);

      sessionId.should.be.ok;
      ws.request(19, sessionId, "universe");
    });
  });

  describe("exception case", function () {
    it("should response with error message", function (done) {
      ws.readyState.should.equal(1);

      var callback = function (data) {
        var message = JSON.parse(data);

        message.shift().should.equal(0);
        message.shift().should.equal("Ungültige Sitzungskennung!");
        ws.removeListener("message", callback);
        done();
      };
      ws.on("message", callback);

      ws.request(13, "invalid sessionId");
    });
  });

  describe("sign out user", function () {
    it("should response success", function (done) {
      ws.readyState.should.equal(1);

      var callback = function (data) {
        var message = JSON.parse(data);

        message.shift().should.equal(1);
        message.shift().should.be.ok;
        sessionId = null;
        ws.removeListener("message", callback);
        done();
      };
      ws.on("message", callback);

      sessionId.should.be.ok;
      ws.request(13, sessionId);
    });
  });

  describe("unregister user", function () {
    it("should response success", function (done) {
      ws.readyState.should.equal(1);

      var callback = function (data) {
        var message = JSON.parse(data);

        switch (message.shift()) {
          case 12:
            sessionId = message.shift();
            sessionId.should.be.ok;
            count++;
            break;
          case 1:
            message.shift().should.be.ok;
            count++;
            break;
          default:
            throw new Error("invalid response type");
        }

        if (count === 2) {
          count = 0;
          ws.removeListener("message", callback);
          done();
        }
      };
      ws.on("message", callback);

      ws.request(10, "beta", "tester");
      setTimeout(function () {
        ws.request(20, sessionId);
      }, 1000);
    });
  });

  describe("send invalid message", function () {
    it("should close websocket", function (done) {
      ws.readyState.should.equal(1);

      var callback = function (code, message) {
        code.should.equal(1000);
        message.should.equal("Unbekannter Nachrichtentyp!");
        ws.removeListener("close", callback);
        done();
      };
      ws.on("close", callback);

      ws.request(999);
    });
  });
});
