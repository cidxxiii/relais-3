var debug           = require("debug")("relais-3:server")
  , fs              = require("fs")
  , https           = require("https")
  , WebSocket       = require("ws")
  , WebSocketServer = require("ws").Server
  , redis           = require("redis")
  , async           = require("async")
  , User            = require("./lib/models").User
  , __              = require("underscore")
  , cheerio         = require("cheerio")
  , argv            = require("optimist").demand("config").argv;

/**
 * Load config file.
 */
var config = require(argv.config);

/**
 * Create secure http server.
 */
var opts = {
  cert: fs.readFileSync(config.https.cert),
  key:  fs.readFileSync(config.https.key)
};

if (config.https.hasOwnProperty("ca")) {
  opts.ca = fs.readFileSync(config.https.ca);
}

var httpsServer = https.createServer(opts);
httpsServer.listen(config.port);
debug("https server listen on port", config.port);

/**
 * Create database connection.
 */
var db = redis.createClient(
  config.redis.port,
  config.redis.host,
  {
    auth_pass: config.redis.auth
  }
);

db.on("connect", function () {
  User.init(db);
  debug("redis datastore connected");
});

db.on("error", function (err) {
  debug("cannot connect to redis datastore", err);
  process.exit(1);
});

/**
 * Prototype for websocket responses.
 */
WebSocket.prototype.response = function () {
  var args = [].slice.call(arguments);

  var response = JSON.stringify(args);
  if (this.readyState !== 1) {
    return debug("could not sent message", response);
  }

  debug("outgoing message", response);
  this.send(response);
};

/**
 * Create websocket server.
 */
var wss = new WebSocketServer({ server: httpsServer });

wss.on("connection", function (ws) {
  debug("new websocket connection (total: " + this.clients.length + ")");

  var channels = function () {
    var list = [];
    list.push("kosmos");
    __.each(wss.clients, function (client) {
      if (client.user && client.user.username) {
        list.push(client.user.username);
      }
    });
    var result = JSON.stringify(list);
    debug("channel list", result);
    return result;
  };

  var broadcast = function () {
    var args = [].slice.call(arguments);

    var broadcast = JSON.stringify(args);
    __.each(wss.clients, function (client) {
      if (client.user && client.readyState === 1) {
        client.send(broadcast);
      }
    });
    debug("outgoing broadcast", broadcast);
  };

  ws.on("close", function (code, message) {
    if (this.clients) {
      debug("websocket disconnected (total: " + this.clients.length + ")", code, message);
    } else {
      debug("no websocket connections");
    }
  });

  ws.on("error", function (err) {
    debug("websocket error", err.message);
    ws.response(0, err.message);
  });

  ws.on("message", function (data) {
    try {
      debug("incoming message", data);
      var message = JSON.parse(data);

      switch (message.shift()) {
        /**
         * Register new user.
         *
         * request  = [10, username, password]
         * response = [12, sessionId]
         */
        case 10:
          var user = new User.model({
            username: message.shift(),
            password: message.shift()
          });
          async.waterfall([
            function (cb) {
              user.create(function (err, reply) {
                if (err) return cb(err);
                cb(null, reply);
              });
            },
            function (reply, cb) {
              reply.authenticate(user.password, function (err, auth) {
                if (err) return cb(err);
                cb(null, auth);
              });
            },
            function (auth, cb) {
              db.SADD("signed-in", auth.id);
              cb(null, auth);
            }
          ], function (err, auth) {
            if (err) return ws.response(0, err.message);
            ws.response(12, auth.sessionId);
            setTimeout(function () {
              broadcast(22, channels());
            }, 500);
            ws.user = auth;
          });
          break;
        /**
         * Authenticate registered user.
         *
         * request  = [11, username, password]
         * response = [12, sessionId]
         */
        case 11:
          var username = message.shift()
            , password = message.shift();

          async.waterfall([
            function (cb) {
              User.findByUsername(username, function (err, reply) {
                if (err) return cb(err);
                cb(null, reply);
              });
            },
            function (reply, cb) {
              if (!reply) return cb(new Error("Benutzer existiert nicht!"));
              reply.authenticate(password, function (err, auth) {
                if (err) return cb(err);
                cb(null, auth);
              });
            },
            function (auth, cb) {
              db.SADD("signed-in", auth.id);
              cb(null, auth);
            }
          ], function (err, auth) {
            if (err) return ws.response(0, err.message);
            ws.response(12, auth.sessionId);
            setTimeout(function () {
              broadcast(22, channels());
            }, 500);
            ws.user = auth;
          });
          break;
        /**
         * Signout user.
         *
         * request  = [13, sessionId]
         * response = [ 1, flashMessage]
         */
        case 13:
          ws.user.check(message.shift(), function (err, valid) {
            if (err) return ws.response(0, err.message);
            if (!valid) return ws.response(0, "Ungültige Sitzungskennung!");

            if (ws.user.sub) {
              ws.user.sub.UNSUBSCRIBE();
              ws.user.sub.QUIT();
              delete ws.user.sub;
            }
            db.SREM("signed-in", ws.user.id);
            ws.response(1, "Komm bald wieder! ;)");
            debug("user signed out", ws.user.username);
            setTimeout(function () {
              broadcast(22, channels());
            }, 500);
            delete ws.user;
          });
          break;
        /**
         * Subscribe to channel.
         *
         * request  = [14, sessionId, channel]
         * response = [15, username, channel, count]
         */
        case 14:
          ws.user.check(message.shift(), function (err, valid) {
            if (err) return ws.response(0, err.message);
            if (!valid) return ws.response(0, "Ungültige Sitzungskennung!");

            if (!ws.user.sub) {
              ws.user.init(function (err, initialized) {
                if (err || !initialized) {
                  return ws.response(0, "Datenbankverbindung konnte nicht hergestellt werden!");
                }
                if (ws.user.sub.listeners("subscribe").length === 0) {
                  ws.user.sub.on("subscribe", function (channel, count) {
                    db.PUBLISH(channel, JSON.stringify({
                      sender: "Eddie",
                      content: "*'" + ws.user.username + "'* ist dem Kanal beigetreten."
                    }), function (err, reply) {
                      if (err) ws.response(0, err.message);
                    });
                    ws.response(15, ws.user.username, channel, count);
                    debug("new subscription", ws.user.username, channel, count);
                  });
                }
                if (ws.user.sub.listeners("message").length === 0) {
                  ws.user.sub.on("message", function (channel, message) {
                    ws.response(18, channel, new Date().getTime(), message);
                    debug("new message", channel, message);
                  });
                }
              });
            }
            ws.user.sub.SUBSCRIBE(message.shift());
          });
          break;
        /**
         * Publish to channel.
         *
         * request  = [16, sessionId, channel, content]
         * response = [17, channel, count]
         */
        case 16:
          ws.user.check(message.shift(), function (err, valid) {
            if (err) return ws.response(0, err.message);
            if (!valid) return ws.response(0, "Ungültige Sitzungskennung!");

            channel = message.shift();
            db.PUBLISH(channel, message.shift(), function (err, count) {
              if (err) return ws.response(0, "Fehler beim Senden der Nachricht!");
              ws.response(17, channel, count);
            });
          });
          break;
        /**
         * Unsubscribe from channel.
         *
         * request  = [19, sessionId, channel]
         * response = [ 1, flashMessage]
         */
        case 19:
          ws.user.check(message.shift(), function (err, valid) {
            if (err) return ws.response(0, err.message);
            if (!valid) return ws.response(0, "Ungültige Sitzungskennung!");

            channel = message.shift();
            if (ws.user.sub) {
              ws.user.sub.UNSUBSCRIBE(channel);
              db.PUBLISH(channel, JSON.stringify({
                sender: "Eddie",
                content: "*'" + ws.user.username + "'* hat den Kanal verlassen."
              }), function (err, reply) {
                if (err) ws.response(0, err.message);
              });
              ws.response(1, "Du hast den Kanal '" + channel + "' verlassen!");
            }
          });
          break;
        /**
         * Delete user.
         *
         * request  = [20, sessionId]
         * response = [ 1, flashMessage]
         */
        case 20:
          ws.user.check(message.shift(), function (err, valid) {
            if (err) return ws.response(0, err.message);
            if (!valid) return ws.response(0, "Ungültige Sitzungskennung!");

            ws.user.delete(function (err, reply) {
              if (err) {
                return ws.response(0, "Fehler beim Löschen deines Benutzerkontos!");
              }
              if (ws.user.sub) {
                ws.user.sub.UNSUBSCRIBE();
                ws.user.sub.QUIT();
                delete ws.user.sub;
              }
              delete ws.user;
              ws.response(1, "Schade, dass du 'sonnenkarma.de' verlassen hast!");
              setTimeout(function () {
                broadcast(22, channels());
              }, 500);
            });
          });
          break;
        /**
         * Send channel list.
         *
         * request  = [21, sessionId]
         * response = [22, userlist]
         */
        case 21:
          ws.user.check(message.shift(), function (err, valid) {
            if (err) return ws.response(0, err.message);
            if (!valid) return ws.response(0, "Ungültige Sitzungskennung!");

            ws.response(22, channels());
          });
          break;
        /**
         * Send image list.
         *
         * request  = [23, sessionId]
         * response = [24, imagelist]
         */
        case 23:
          ws.user.check(message.shift(), function (err, valid) {
            if (err) return ws.response(0, err.message);
            if (!valid) return ws.response(0, "Ungültige Sitzungskennung!");

            db.ZRANGE("images", 0, -1, function (err, reply) {
              if (err) return ws.response(0, err.message);

              var images = JSON.stringify(reply);
              debug("imagelist", images);
              ws.response(24, images);
            });
          });
          break;
        /**
         * Add image to list.
         *
         * request  = [25, sessionId, image]
         * response = [ 1, flashMessage]
         * broadcast= [26, image]
         */
        case 25:
          ws.user.check(message.shift(), function (err, valid) {
            if (err) return ws.response(0, err.message);
            if (!valid) return ws.response(0, "Ungültige Sitzungskennung!");

            var link = message.shift()
              , $    = cheerio.load(link)
              , url  = null
              , desc = "Keine Beschreibung";

            if ($("img").length > 0) {
              url = $("img").attr("src");
              if ($("img").attr("alt").length) {
                desc = $("img").attr("alt");
              }
            } else if ($("a").length > 0) {
              return ws.response(0, "Ungültiger Link!");
            } else {
              url = link;
            }

            var regex = new RegExp("[a-zA-Z\d]+://(\w+:\w+@)?([a-zA-Z\d.-]+\.[A-Za-z]{2,4})(:\d+)?(/.*)?");
            if (!regex.test(url)) {
              return ws.response(0, "Ungültiger Link!");
            }

            url = "![" + desc + "](" + url + ")";

            db.ZADD("images", 1, url, function (err, reply) {
              if (err) return ws.response(0, err.message);

              debug("added images", reply);
              ws.response(1, "Bild hinzugefügt!");
              setTimeout(function () {
                broadcast(26, url);
              }, 500);
            });
          });
          break;
        /**
         * Close socket on invalid messages.
         */
        default:
          ws.close(1000, "Unbekannter Nachrichtentyp!");
      }
    /**
     * Send error message.
     */
    } catch (ex) {
      ws.response(0, ex.message);
      debug("message processing error", ex.message);
    }
  });
});

wss.on("error", function (err) {
  debug("websocket server error", err);
});
